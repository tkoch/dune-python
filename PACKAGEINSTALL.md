If have installed all your dune modules,
it is possible to have the dune-py directory be created automatically during the
import of the dune python package into python. This requires setting
the following environment variables:

- `DUNE_CONTROL_PATH`: As for dunecontrol itself, this is a colon-separated
  list of directories to look for dune.module files, i.e., Dune source
  modules. It suffices to provide a parent directory to Dune modules.
  Installed modules will not be found automatically.  Rather, DUNE_CONTROL_PATH
  needs to include this global installation as well.  On a Debian system, globally
  installed modules are in `/usr/lib/dunecontrol`.
- `DUNE_CMAKE_FLAGS`: List of CMake definitions similar to the one in your opts
  file. You may omit the `-D` though.
- `DUNE_PY_DIR`: location where the `dune-py` module should be placed. This
  defaults to `~/.cache`.

Unfortunately, the automatic generation of the dune-py module still requires
some configuration (at least for now), which is described below.

If you installed the Dune modules somewhere, make sure this path is in your
`PKG_CONFIG_PATH` environment variable. For example, if you installed into a
virtual env, you can use
```
export PKG_CONFIG_PATH=${VIRTUAL_ENV}/lib/pkgconfig:${PKG_CONFIG_PATH}
```
from within the virtual env.

Environment Variables Summary
-----------------------------

- `DUNE_CONTROL_PATH`: Where to look for DUNE source modules (can be a parent directory).
  Required if you don't create dune-py manually.
- `PKG_CONFIG_PATH`: Where to look for pkg-config files.
  Required if you don't create dune-py manually and your DUNE is installed.
- `DUNE_CMAKE_FLAGS`: Definitions to pass to CMake.
  Required if you don't create dune-py manually.
- `DUNE_PY_DIR`: location where the `dune-py` module should be placed. This
  defaults to `~/.cache`.
