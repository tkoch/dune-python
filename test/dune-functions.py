import numpy
import numpy.random

import dune.common
from dune.grid import ugGrid
from dune.functions import defaultGlobalBasis, Lagrange

grid = ugGrid((dune.common.reader.structured, "simplex", [0.0, -0.5], [1.0, 0.5], [31, 31]), dimgrid=2)
element = grid.elements.__next__()
x1 = [0.1,0.1]
x3 = numpy.array([ [0,0.2,0.4],[0.5,0.5,0.1] ])

basis = defaultGlobalBasis(grid, Lagrange(order=1)**2)

dofVector = numpy.random.random(basis.dimension);
gf = basis.asFunction(dofVector)
loc = gf.localFunction()
loc.bind(element)

y  = loc(x1)
y -= gf(element,x1)
assert type(y) == type(dune.common.FieldVector([0,0]))
assert y.two_norm < 1e-13
y  = loc( x3 )
y -= gf(element,x3)
assert y.shape == (2,3)
assert numpy.sum(y*y) < 1e-13

loc.unbind()
