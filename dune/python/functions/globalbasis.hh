#ifndef DUNE_PYTHON_FUNCTIONS_SPACE_HH
#define DUNE_PYTHON_FUNCTIONS_SPACE_HH

#include <cstddef>

#include <tuple>
#include <type_traits>
#include <utility>

#include <dune/functions/functionspacebases/defaultglobalbasis.hh>

#include <dune/python/common/dimrange.hh>
#include <dune/python/common/fmatrix.hh>
#include <dune/python/common/fvector.hh>
#include <dune/python/functions/discretefunction.hh>
#include <dune/python/functions/interpolate.hh>
#include <dune/python/functions/tree.hh>

#include <dune/python/pybind11/complex.h>
#include <dune/python/pybind11/pybind11.h>


namespace Dune
{

  namespace Python
  {

    // MultiIndex
    // ----------

    template< class FactoryTag >
    using MultiIndex = Dune::ReservedVector< std::size_t, FactoryTag::requiredMultiIndexSize >;



    // NodeFactory
    // -----------

    template< class GridView, class FactoryTag >
    using NodeFactory = std::decay_t< decltype( std::declval<FactoryTag>().template build< MultiIndex< FactoryTag > >( std::declval<GridView>() ) ) >;



    // DefaultGlobalBasis
    // ------------------

    template< class GridView, class FactoryTag >
    struct DefaultGlobalBasis
      : public Dune::Functions::DefaultGlobalBasis< NodeFactory< GridView, FactoryTag > >
    {
      typedef Dune::Functions::DefaultGlobalBasis< NodeFactory< GridView, FactoryTag > > Base;

      explicit DefaultGlobalBasis ( const GridView &gridView )
        : Base( FactoryTag().template build< MultiIndex< FactoryTag > >( gridView ) )
      {}
    };



    // registerGlobalBasis
    // -------------------

    template <class Basis>
    struct LocalViewWrapper : public Basis::LocalView
    {
      typedef typename Basis::LocalView Base;
      typedef typename Base::Element EntityType;
      LocalViewWrapper(const Basis &b) : Base(b) {}
      void bind ( pybind11::object &obj )
      {
        obj_ = obj;
        const EntityType &entity = obj.template cast<const EntityType&>();
        Base::bind(entity);
      }
      void unbind ( )
      {
        Base::unbind();
        obj_.release();
      }
      pybind11::object obj_;
    };
    template <class Basis>
    struct LocalIndexWrapper
    {
      typedef typename Basis::LocalView View;
      LocalIndexWrapper(const Basis &basis) : indexSet_(basis.localIndexSet()) {}
      std::vector<int> index(int index) const
      {
        auto ind = indexSet_.index(index);
        std::vector<int> ret(ind.size());
        for (int i=0;i<ind.size();++i) ret[i] = ind[i];
        return ret;
      }

      void bind ( pybind11::object &obj )
      {
        obj_ = obj;
        const LocalViewWrapper<Basis> &view = obj.template cast<const LocalViewWrapper<Basis>&>();
        indexSet_.bind(view);
      }
      void unbind ( )
      {
        indexSet_.unbind();
        obj_.release();
      }
      pybind11::object obj_;
      typename Basis::LocalIndexSet indexSet_;
    };

    template< class GlobalBasis, class... options >
    DUNE_EXPORT void registerGlobalBasis ( pybind11::module module, pybind11::class_< GlobalBasis, options... > &cls )
    {
      using pybind11::operator""_a;

      typedef Dune::TypeTree::HybridTreePath<> DefaultTreePath;

      const std::size_t dimRange = DimRange< typename GlobalBasis::NodeFactory::template Node< DefaultTreePath > >::value;

      cls.def( pybind11::init( [] ( const typename GlobalBasis::GridView &gridView ) { return new GlobalBasis( gridView ); } ), pybind11::keep_alive< 1, 2 >() );
      cls.def( "__len__", [](const GlobalBasis& self) { return self.dimension(); } );

      cls.def_property_readonly( "dimRange", [] ( pybind11::handle self ) { return pybind11::int_( dimRange ); } );
      cls.def_property( "gridView",
                        [](const GlobalBasis& basis) { return basis.gridView(); },
                        [](GlobalBasis& basis, const typename GlobalBasis::GridView& gridView) { basis.update(gridView); });

      typedef LocalViewWrapper< GlobalBasis > LocalView;
      auto lv = insertClass< LocalView >( module, "LocalView",
          GenerateTypeName("Dune::Python::LocalViewWrapper", MetaType<GlobalBasis>()),
          IncludeFiles{"dune/python/functions/globalbasis.hh"}).first;
      lv.def( "bind", &LocalView::bind );
      lv.def( "unbind", &LocalView::unbind );
      lv.def( "__len__", [] ( LocalView &self ) -> int { return self.size(); } );

      Functions::registerTree<typename LocalView::Tree>(lv);
      lv.def("tree", [](const LocalView& view) { return view.tree(); });

      typedef LocalIndexWrapper< GlobalBasis > LocalIndex;
      auto lis = insertClass<LocalIndex>(module, "LocalIndex",
            GenerateTypeName("Dune::Python::LocalIndexWrapper", MetaType<GlobalBasis>()),
            IncludeFiles{"dune/python/functions/globalbasis.hh"}).first;
      lis.def( "bind", &LocalIndex::bind );
      lis.def( "unbind", &LocalIndex::unbind );
      lis.def( "index", [] ( const LocalIndex &localIndexSet, int index ) { return localIndexSet.index( index ); });

      cls.def( "localView", [] ( const GlobalBasis &self ) -> LocalView { return LocalView( self ); }, pybind11::keep_alive< 0, 1 >() );
      cls.def( "localIndexSet", [] ( const GlobalBasis &self ) -> LocalIndex { return LocalIndex( self ); }, pybind11::keep_alive< 0, 1 >() );
      cls.def_property_readonly( "dimension", [] ( const GlobalBasis &self ) -> int { return self.dimension(); } );

      cls.def( "interpolate", &Dune::Python::Functions::interpolate<GlobalBasis, double> );
      cls.def( "interpolate", &Dune::Python::Functions::interpolate<GlobalBasis, bool> );
      cls.def( "interpolate", &Dune::Python::Functions::interpolate<GlobalBasis, int> );

      typedef Dune::FieldVector< double, dimRange > Range;
      typedef Dune::Functions::DiscreteGlobalBasisFunction< GlobalBasis, DefaultTreePath, HierarchicPythonVector< double >, DefaultNodeToRangeMap< GlobalBasis, DefaultTreePath >, Range > DiscreteFunction;
      // TODO use insertClass
      static pybind11::class_< DiscreteFunction > clsDiscreteFunction( cls, "DiscreteFunction" );
      registerDiscreteFunction( cls, clsDiscreteFunction );

      cls.def("asFunction", [] ( GlobalBasis &self, pybind11::buffer dofVector ) {
          auto nodeToRangeMapPtr = std::make_shared< DefaultNodeToRangeMap< GlobalBasis, DefaultTreePath > >( makeDefaultNodeToRangeMap( self, DefaultTreePath() ) );
          auto basisPtr = Dune::wrap_or_move( self );
          auto vectorPtr = std::make_shared< HierarchicPythonVector< double > >( dofVector );
          return new DiscreteFunction( basisPtr, DefaultTreePath(), vectorPtr, nodeToRangeMapPtr );
        }, pybind11::keep_alive< 0, 1 >(), pybind11::keep_alive< 0, 2 >(), "dofVector"_a );
    }

  } // namespace Python

} // namespace Dune

#endif // #ifndef DUNE_PYTHON_FUNCTIONS_SPACE_HH
