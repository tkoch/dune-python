.. contents::
    :local:
    :backlinks: top

.. _usageintro:

#################################
Introduction
#################################

This document aims to help the user of `dune-python`. It describes how to use the grid interface of `Dune` with `dune-python`, in Python. 

.. _usagecreateagrid:

#################################
Creating a grid
#################################

.. code-block:: python

    import dune.grid as grid

    yaspgrid_m = grid.get("YaspGrid", dimgrid=2, dimworld=2)
    gridView   = grid.leafGrid("../data/unitcube-2d.dgf", yaspgrid_m)

First, you have to import the :code:`dune.grid` Python module that manages grid constructions and provides some important functions. Then, use the :code:`get` method with the grid parameters you want (name, dimension of your grid, dimension of your world, etc.) to create a Python grid module that exports all of the grid interface parameterized by the type of grid you just provided (eg :code:`YaspGrid`).
Finally, you can create your leaf grid by calling the :code:`leafGrid` function with a DGF file and the Python grid module you have just generated.

That is the classic way to create a grid with `dune-python`. If you want to know more about the generation of Python grid modules, see :doc:`construction`. 



.. _usagephilosophy:

#################################
Philosophy
#################################

Once you have created your grid according to the previous part, the idea is to use it like you would do with the C++ interface. The philosophy is to respect the C++ interface as far as possible while providing some Python idioms.

Here is a general list of things that demonstrate that:

* (python) names follow the CamelCase convention like the C++ interface does

* C++ methods with no parameter are exported as Python properties.
    Example: given an entity :code:`e`, you would obtain the geometric realization of that entity with :code:`e.geometry()` in C++. Thus, you would write :code:`e.geometry` in Python.

* classes and functions that depend on a grid/gridView and can be called from the :code:`Dune` namespace are in the Python module generated when you have created your grid.
    Example: if you want to use the :code:`MultipleCodimMultipleGeometryTypeMapper` that is in the :code:`Dune` namespace and can be accessed as following: :code:`Dune::MultipleCodimMultipleGeometryTypeMapper` in C++, you would write :code:`yaspgrid_m.MultipleCodimMultipleGeometryTypeMapper` in Python (using the :code:`yaspgrid_m` module previously generated, see above).

* C++ iterators are exported as Python iterators
    Example: given a gridView, you can iterate through the leaf elements (codim = 0) by using C++ iterators and a `for` loop like that:

    .. code-block:: c++

        LeafIterator endit = gridView.template end<0>();
        for (LeafIterator it = gridView.template begin<0>(); it!=endit; ++it)
        {
            ...
        }

    To iterate through these leaf elements in Python, you just have to use the :code:`elements` iterator exported by `dune-python`:

    .. code-block:: python

        for e in gridView.elements():
            ...
    For more details about the iterators exported by `dune-python`, see :ref:`usageiterators`.

* template parameters on some C++ objects are transformed into Python function parameters (when it makes sense)
    Example: :code:`Dune::ReferenceElements<ctype, dim>` can be used as following, in Python: :code:`gridModule.ReferenceElements(dim)` where :code:`ctype` template parameter is inferred from the grid. You clearly see that the compile-time parameter `dim` has been transformed into a runtime parameter.

* as a Python user, you should use Python data structures (like :code:`list` or `Numpy` arrays) instead of `Dune` containers (like :code:`FieldVector`, :code:`FieldMatrix`, :code:`DynamicVector`, :code:`DynamicMatrix`, etc.). See the below section :ref:`usagedatastructures` and the :ref:`usagefiniteelements` for a detailed example.



.. _usagedatastructures:

#################################
Data structures
#################################

`Dune` defines its own containers like :code:`FieldVector` and :code:`FieldMatrix`. However, as `dune-python` wants to provide a `Pythonic` way to use `Dune`, you can use Python containers instead of Dune's. Indeed, thanks to `pybind11`, there is a transparent support of these `Dune` types through Python lists or Numpy arrays.

Take for example :code:`Dune::FieldVector`, you can find the following code in the finite volume implementation:

.. code-block:: python

    velocity = u(faceglobal, t)
    factor = velocity * integrationOuterNormal / volume

where :code:`velocity` is actually the Python list :code:`[1.0, 1.0]` and :code:`integrationOuterNormal` is a :code:`Dune::FieldVector` returned by the :code:`centerUnitOuterNormal` method. You can note that the dot product :code:`*` is managed without you caring about any explicitly conversion between :code:`Python list` and :code:`Dune::FieldVector`. In a similar way, if you want to use a `Dune` method that take a :code:`FieldVector` as argument, you can pass a Python list or a Numpy
array.

Likewise, you can manipulate :code:`Dune::FieldMatrix` as a Numpy matrix, see the following example (used in the :ref:`usagefiniteelements`):

.. code-block:: python

    jacInvTra = np.array(e.geometry.jacobianInverseTransposed(p.position), copy = False)

    grad2 = np.empty(jacInvTra.shape[1])
    jacInvTra.dot(basis[j].evaluateGradient(p.position), grad2)

Here, :code:`e.geometry.jacobianInverseTransposed(p.position)` returns a :code:`Dune::FieldMatrix`. To manipulate it, just initialize a `Numpy` array with it, (it's transparent because of `pybind11` and the `buffer protocole` and thus, you can avoid copies by specifying :code:`copy = False` like in the example). You can then use thix matrix as a classic `Numpy` matrix. For example, if you want to apply the dot product between this matrix and the :code:`Dune::FieldVector` returned by
:code:`basis[j].evaluateGradient(p.position)`, you just have to use the `Numpy` :code:`dot` method and provide an empty `Numpy` array to save the result, see above. 

.. _usageiterators:

#################################
Iterators
#################################

Please refer to the `Iterating over grid entities and intersections`_ page.

`dune-python` defines the following mapping:

+-------------------------------------------------------------------------------------+-----------------------------------------------+
| C++ range:                                                                          | Python iterator:                              |
+=====================================================================================+===============================================+
| :code:`elements(const GV& gv)`                                                      | :code:`gridView.elements()`                   |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`elements(const GV& gv, PartitionSet<partitions> ps)`                         | :code:`gridView.elements(partitionSet)`       |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`facets(const GV& gv)`                                                        | :code:`gridView.facets()`                     |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`facets(const GV& gv, PartitionSet<partitions> ps)`                           | :code:`gridView.facets(partitionSet)`         |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`edges(const GV& gv)`                                                         | :code:`gridView.edges()`                      |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`edges(const GV& gv, PartitionSet<partitions> ps)`                            | :code:`gridView.edges(partitionSet)`          |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`vertices(const GV& gv)`                                                      | :code:`gridView.vertices()`                   |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`vertices(const GV& gv, PartitionSet<partitions> ps)`                         | :code:`gridView.vertices(partitionSet)`       |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`intersections(const GV& gv, const Entity& e)`                                | :code:`gridView.intersections(entity)`        |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`descendantElements(const Entity& e, int maxLevel)`                           | :code:`entity.descendants`                    |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`entities(const GV& gv, Codim<codim> cd)`                                     | :code:`gridView.entities(codim)`              |
+-------------------------------------------------------------------------------------+-----------------------------------------------+
| :code:`entities(const GV& gv, Codim<codim> cd, PartitionSet<partitions> ps)`        | :code:`gridView.entities(codim, partitionSet)`|
+-------------------------------------------------------------------------------------+-----------------------------------------------+

Please note that :code:`entity.descendants` is only available for entities of codimension 0 (thus, just for the leaf elements).

`partitionSet` is a `PartitionIteratorType` (see the `documentation`_) value, reachable from your grid module. So, given your grid module `grid_m`, it can take any of the following value:

* `grid_m.Interior_Partition`
* `grid_m.InteriorBorder_Partition`
* `grid_m.Overlap_Partition`
* `grid_m.OverlapFront_Partition`
* `grid_m.All_Partition`
* `grid_m.Ghost_Partition`

.. _Iterating over grid entities and intersections: https://www.dune-project.org/doc/doxygen/html/group__GIIteration.html
.. _documentation: https://beta.dune-project.org/doxygen/master/group__GIRelatedTypes.html#gaa5b9e8102d7f70f3f4178182629d98b6


#################################
Mappers
#################################

If you want to use a mapper to attach data to your grid, you can use the exported :code:`Dune::MultipleCodimMultipleGeometryTypeMapper`. The :code:`MultipleCodimMultipleGeometryTypeMapper` class is available in your grid module (eg :code:`yaspgrid_m`, see :ref:`usagecreateagrid`) and you can create your mapper by providing a grid view and a layout (a callable) like the following:

.. code-block:: python

    gridModule.MultipleCodimMultipleGeometryTypeMapper(gridView, lambda gt: gt.dim == gridView.dimension)

The layout, here a lambda function, takes the geometry type of an entity as parameter and should return a bool depending on you want attach data to this entity or not. Here for example, you are attaching data only if :code:`gt.dim == gridView.dimension` and thus, you are only attaching data on leaf elements. If you want to attach data to vertices, you should use :code:`gt.dim == 0`, :code:`gt.dim == 1` for edges, etc.



#################################
VTK output
#################################

`dune-python` provides support to VTK output: you can write arbitrary grid functions (although living on the cells or the vertices of your grid) to a file suitable for easy visualization (eg a VTU file that you can open with ParaView).

.. code-block:: python

    vtkwriter = gridView.vtkWriter()
    lgf = gridView.localGridFunction("data", lambda element, _: [data[mapper.index(element)]])
    lgf.addToVTKWriter(vtkwriter, vtkwriter.DataType.CellData)

    ...

    vtkwriter.write("nameOfYourOutputFile", vtkwriter.OutputType.ascii)

Here, you have a :code:`data` array that you have mapped to the cells of your grid with the use of :code:`mapper` (a :code:`MultipleCodimMultipleGeometryTypeMapper` mapper, precisely) and you want to produce a VTK output of your grid with its attached data.

First, you have to create an instance of the :code:`Dune::VTKWriter` (see https://www.dune-project.org/doc/doxygen/html/classDune_1_1VTKWriter.html). Then, define a local grid function that basically maps an element of your grid to its attached data. This local grid function takes its name as first parameter and a callable as second parameter. Please note the following constraints on this callable:

.. DANGER::
    Your callable has to take two parameters: an element and its (local) position and has to return its attached data as a **list** (of size 1 in this example)

After defining this local grid function, add it to your vtkwriter like here: :code:`lgf.addToVTKWriter(vtkwriter, vtkwriter.DataType.CellData)`. You can finally generate a VTU file each time you want by calling the :code:`write` method of your :code:`vtkwriter` with a name for your VTU file and an argument to specify the encoding (eg :code:`vtkwriter.OutputType.ascii`).

The idea is the same if you have data attached to the vertices of your grid, but there are some little differences, see the following:

.. code-block:: python

    f = grid.P1VTKFunction(yaspgrid_m, gridView, data)
    lgf = gridView.localGridFunction("data", f.evaluate)
    lgf.addToVTKWriter(vtkwriter, vtkwriter.DataType.PointData)

    vtkwriter.write("nameOfYourOutputFile", vtkwriter.OutputType.ascii)

Here you have to use the :code:`P1VTKFunction` class (from your grid module) to transform your :code:`data` array into a local grid function. Once you have created your :code:`P1VTKFunction` object (eg :code:`f`), just pass its callable :code:`f.evaluate` to the :code:`gridView.localGridFunction` function to get your local grid function. Then, like the previous example, add it to your vtkwriter but using :code:`vtkwriter.DataType.PointData` this time (to attach your data to the
vertices). You can finally generate your output files as previously.

.. _usagefiniteelements:

#################################
Finite elements example
#################################

Here is a detailed example of what you can do with `dune-python`. The following code is an implementation of the finite elements scheme described in the `dune-grid-howto` PDF and implemented in C++ here: https://gitlab.dune-project.org/core/dune-grid-howto/blob/master/finiteelements.cc. It follows the :ref:`usagephilosophy` described in the above section ; in particular, please note the use of Python data structures like `Numpy` arrays or sparse matrices (see lines 33 and 34) and the use of a `SciPy` preconditionner and solver (see lines 112, 113 and 116) to solve the problem.

.. literalinclude:: ../../demo/finiteelements.py
    :linenos:
    :emphasize-lines: 33, 34, 112, 113, 116
