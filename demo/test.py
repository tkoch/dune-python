import logging
import shutil
import dune.common.module
logger = logging.getLogger("dune.common")

logformat='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=logformat, level="DEBUG", filename="test.out")

try:
    pydir = dune.common.module.get_dune_py_dir()
    try:
        shutil.rmtree(pydir)
    except:
        pass
    import dune.generator
    print("successful")
except Exception as e:
    print("configuration did not work: ",e)
