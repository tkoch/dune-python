import sys
import math
import dune.geometry
import numpy

from dune.geometry.quadpy import rules as quadpyRules, rule as quadpyRule
from dune.grid import gridFunction

def _test(grid,f):
    try:
        e = grid.elements.next()
    except AttributeError:
        e = grid.elements.__next__()

    # first: the pedestrian way to do the quadrature
    duneQR = dune.geometry.quadratureRule(e.type,5)
    if e.type.isCube:
        quadpyQR = quadpyRule(e.type,("C2 5-2","Stroud"))
    else:
        quadpyQR = quadpyRule(e.type,(3,"XiaoGimbutas"))

    dune_quad = 0
    print("Dune: points and weights")
    for q in duneQR:
        print(q.position, q.weight)
        dune_quad += f(e.geometry.toGlobal(q.position))*\
                     e.geometry.integrationElement(q.position)*q.weight
    print("order of dune quadrautre=",duneQR.order)
    quadpy_quad = 0
    print("Quadpy: points and weights")
    for q in quadpyQR:
        print(q.position, q.weight)
        quadpy_quad += f(e.geometry.toGlobal(q.position))*\
                       e.geometry.integrationElement(q.position)*q.weight
    print("order of quadpy quadrautre=",quadpyQR.order)

    # second: the fancy way to do the quadrature
    duneQRs = dune.geometry.quadratureRules(5)
    quadpyQRs = quadpyRules(
            {dune.geometry.cube(2): ("C2 5-2","Stroud"),
             dune.geometry.simplex(2): (5, "XiaoGimbutas")
            } )

    print("Results on single element:")
    print("Dune explicit quadrature: int_re f=",dune_quad)
    print("quadpy explicit loop: int_re f=    ",quadpy_quad)
    print("dune integrate: int_re f=          ",dune.geometry.integrate(duneQRs,e,f))
    print("quadpy integrate: int_re f=        ",dune.geometry.integrate(quadpyQRs,e,f))

    dune_quad = 0
    quadpy_quad = 0
    for e in grid.elements:
        dune_quad += dune.geometry.integrate(duneQRs,e,f)
        quadpy_quad += dune.geometry.integrate(quadpyQRs,e,f)
    print("Results on domain:")
    print("Dune   =",dune_quad)
    print("quadpy =",quadpy_quad)

def test(grid):
    @gridFunction(grid)
    def f(x): return x[0]*x[1]*numpy.sum(numpy.sin(x[:]),axis=0)
    _test(grid,f)
    @gridFunction(grid)
    def f(x): return numpy.array([ c*x[0]*x[1]*numpy.sum(numpy.sin(x[:]),axis=0)\
                for c in range(3) ])
    _test(grid,f)


import dune.grid
grid = dune.grid.structuredGrid([0,0],[1,1],[3,3])
test(grid)
print("===========================================++")
print("===========================================++")
print("===========================================++")
import dune.alugrid
grid = dune.alugrid.aluConformGrid(dune.grid.cartesianDomain([0,0],[1,1],[3,3]))
test(grid)
sys.exit(0)
print("===========================================++")
print("===========================================++")
print("===========================================++")
grid = dune.alugrid.aluConformGrid(dune.grid.cartesianDomain([0,0,0],[1,1,1],[3,3,3]))
test(grid)
print("===========================================++")
print("===========================================++")
print("===========================================++")
# grid = dune.grid.structuredGrid([0,0,0],[1,1,1],[3,3,3])
# test(grid)
