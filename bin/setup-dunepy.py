#!/usr/bin/env python
from __future__ import absolute_import, division, print_function, unicode_literals

import getopt
import os
import shlex
import subprocess
import sys
import shutil
import logging

logger = logging.getLogger(__name__)

try:
    from dune.common.module import build_dune_py_module, get_dune_py_dir, make_dune_py_module, select_modules
except ImportError:
    import os
    here = os.path.dirname(os.path.abspath(__file__))
    mods = os.path.join(os.path.dirname(here), "python", "dune", "common")
    if os.path.exists(os.path.join(mods, "module.py")):
        sys.path.append(mods)
        from module import build_dune_py_module, get_dune_py_dir, make_dune_py_module, select_modules
    else:
        raise

def buffer_to_str(b):
    return b if sys.version_info.major == 2 else b.decode('utf-8')

def toBuildDir(builddir, moddir, module):
    if os.path.isabs(builddir):
        return os.path.join(builddir ,module)
    else:
        return os.path.join(moddir, builddir)

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"ho",["opts="])
    except getopt.GetoptError:
        print('usage: setup-dunepy.py [-o config.opts | --opts=config.opts] [install]')
        sys.exit(2)

    optsfile = None
    for opt, arg in opts:
        if opt == '-h':
            print('usage: setup-dunepy.py [-o config.opts | --opts=config.opts] [install]')
            sys.exit(2)
        elif opt in ("-o", "--opts"):
            optsfile = arg
    if len(args) > 0:
        execute = args[0]
    else:
        execute = ""

    if optsfile is not None:
        definitions = {}
        command = ['bash', '-c', 'source ' + optsfile + ' && echo "$CMAKE_FLAGS"']
        proc = subprocess.Popen(command, stdout = subprocess.PIPE)
        stdout, _ = proc.communicate()
        for arg in shlex.split(buffer_to_str(stdout)):
            key, value = arg.split('=', 1)
            if key.startswith('-D'):
                key = key[2:]
            definitions[key] = value
        command = ['bash', '-c', 'source ' + optsfile + ' && echo "$BUILDDIR"']
        proc = subprocess.Popen(command, stdout = subprocess.PIPE)
        stdout, _ = proc.communicate()
        builddir = buffer_to_str(stdout).strip()
        if not builddir:
            builddir = 'build-cmake'
    else:
        definitions = None
        builddir = 'build-cmake'

    dunepy = get_dune_py_dir()

    if os.path.exists(dunepy):
        shutil.rmtree(dunepy)
    foundModule = make_dune_py_module(dunepy)

    output = build_dune_py_module(dunepy, definitions)

    print("CMake output")
    print(output)

    # set a tag file to avoid automatic reconfiguration in builder
    tagfile = os.path.join(dunepy, ".noconfigure")
    f = open(tagfile, 'w')
    f.close()

    if execute == "install":
        duneModules = select_modules()
        moddir = duneModules[1]["dune-python"]
        pythonModules = [ toBuildDir(builddir,moddir,'dune-python') ]
        for m,depends in duneModules[0].items():
            moddir = duneModules[1][m]
            for d,v in depends.depends:
                if d == 'dune-python':
                    pythonModules = pythonModules + [ toBuildDir(builddir,moddir,m) ]
            for d,v in depends.suggests:
                if d == 'dune-python':
                    pythonModules = pythonModules + [ toBuildDir(builddir,moddir,m) ]
        for moddir in pythonModules:
            print("calling install_python in",moddir)
            command = ['cmake', '--build', '.', '--target', 'install_python']
            proc = subprocess.Popen(command, cwd=moddir, stdout = subprocess.PIPE)
            stdout, stderr = proc.communicate()
            logger.debug(buffer_to_str(stdout))

if __name__ == "__main__":
    main(sys.argv[1:])
