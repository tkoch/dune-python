from __future__ import print_function

import sys

try:
    from dune.common.module import *
except ImportError:
    import os
    here = os.path.dirname(os.path.abspath(__file__))
    mods = os.path.join(os.path.dirname(here), "python", "dune", "common")
    if os.path.exists(os.path.join(mods, "module.py")):
        sys.path.append(mods)
        from module import *
    else:
        raise

definitions={}
for arg in sys.argv[1:]:
    key, value = arg.split('=',1)
    definitions[key] = value

modules, dirs = select_modules()
deps = resolve_dependencies(modules)
order = resolve_order(deps)
prefix = {}
for name in order:
    dir = dirs[name]
    if is_installed(dir, name):
        prefix[name] = get_prefix(name)
    else:
        builddir = default_build_dir(dir, name)
        print("Going to build " + name + "...")
        output = configure_module(dir, builddir, {d: prefix[d] for d in deps[name]}, definitions)
        print(output)
        output = build_module(builddir)
        prefix[name] = builddir
