Differences and Extensions to the Dune C++ Interface
====================================================

The following give a short overview of changes and extensions we made to the
Dune interface while exporting it to Python. Some changes are required due to the
language restriction in Python or to make the resulting interface more Pythonic.
Other changes are necessary to make writing efficient code possible.
Overall this summary is aimed at avoiding surprises to Dune developers familiar with
the C++ interface.

Modification of interface methods
---------------------------------

-   Since `global` is a keyword in Python we can not export the
    `global` method on the `Geometry` directly. So we have
    exported `global` as `position` and `local` as `localPosition`.
-   In many places we replaced methods with properties, i.e.,
    `entity.geometry` instead of `entity.geometry()`.
-   Methods returning a `bool` specifying that other interface methods will
    return valid results are not exported (e.g. `neighbor` on the intersection class)
    instead `None` is returned to specify a non valid call (e.g. to `outside`).
-   Iterators and partitions (TODO, consider putting this under extensions?)
-   On some interface classes there exists pairs of methods where the
    method with the 'plural name' returns an integer (the 'number of')
    and the singular version takes an integer and returns the 'ith'
    element.

    Examples are `geometry.corners()`, `geometry.corner(i)` or
    `entity.subEntities(codim)`, `entity.subEntity(codim,i)`. Since
    writing loops with these methods incurs a large number of Python->C++
    calls (and is not very Pythonic), we decides to slightly change the
    semantics of these methods: the plural version now returns a list
    object over which one can iterate. The singular version still exists
    in its original form. So instead of

    ~~~
    for i in range(geometry.corners()):
        print( geometry.corner(i) )
    ~~~

    one can simply write:

    ~~~
    for c in geometry.corners:
        print( c )
    ~~~

    Note that to obtain the original value returned by `geometry.corners()`
    one need to write `len( geometry.corners )`.

Vectorization
-------------

We added methods to allow for vectorization using `numpy` arrays.

For example the method `geometry.corner()` (note without the
integer argument `i`)
returns a `numpy` array containing the coordinates of all the corners
in the form `numpy.array([ [x1,x2,..,xN], [y1,y2,...,yN] ])`
Evaluating the function `x*y` is as easy as

~~~
f = lambda p: p[0]*p[1]
y = f(geometry.corner())
~~~

which is equivalent to

~~~
for i in range( len(geometry.corners) ):
    y[i] = f(geometry.corner(i))
~~~

Note that `geometry.corner().transposed()` returns a structure with a similar layout
the return value of `geometry.corners` except that the later is a list of
`FieldVector` while the former is a 2d `numpy` array of shape `[numCorners,dim]`.

_Note_: if a function is to support vectorization then special method
on the `FieldVector` need to be avoided, i.e.,

~~~
f = lambda p: 0 if p.two_norm>1 else 0
~~~

will fail if `p` is a `numpy` vector.

So far all methods on the `geometry` are vectorized, i.e.,
`geometry.position( numpy.array([[0.5,0,0.5],[0,0.5,0.5]])` will
return a vector with the global coordinates of the three edge midpoints
of a geometry over the reference triangle.

The `QuadratureRules` export the quadrature points directly as
`numpy` array (using the Python buffer protocol, i.e.,

~~~
quad = dune.geometry.quadratureRules(entity,type(),order)
y = f( geometry.position( quad ) )
~~~

returns the values of the function `f` at all quadrature points.

In addition `quad.weight` returns a `numpy` array with all the
weights. Using the full vectorization support available,
the integral of `f` over `enttiy` can be computed by

~~~
f(geometry.position(quad)) * quad.weights * geometry.integrationElement(quad)
~~~

Further Interface Extensions:
-----------------------------

-   A `grid` in `dune-python` is always the `LeafGridView` of the hierarchical grid
    To work with the actual hierarchy, i.e., to refine the grid, use the `hierarchicalGrid`
    property.
-   There are additional ways to construct a grid (e.g. providing a `dict` with
    `vertices`, `simplices` etc). Refer to the provided demonstration scripts for details.
-   (Partition) iterators (TODO, or put this at the beginning?)
-   Special intersection iterators (TODO)
-   `dune-python` provides some helpful extensions for using the `MCMGMapper` to
    attach data to the grid. A `MCMGMapper` can be constructed using the `mapper`
    method on the `GridView` class passing in the `Layout` as argument.
-   The mapper class has an additional call method taking an entity, which returns an
    array with the indices of all dofs attached to that entity.
-   A list of dof vectors based on the same mapper can be communicated using
    methods defined on the mapper itself and without having to define a
    `DataHandle` class. To communicate data over a `From` partition to a `To`
    partition use

    ~~~
    mapper.communicate_To_From(op, v1,v2,..,vN)
    ~~~

    Here `v1,..,vN` are vectors of dofs and `op` is a lambda of the form:
    `op = lambda local,remote: returnValue`.

    Standard operations are predefined and can be used based on tags:
    `mapper.communicate_InteriorBorder_All(dune.grid.add,v)` is equivalent to
    `gridView.communicate(dune.grid.dataHandle(dune.grid.add,mapper, v),
              dune.common.InterfaceType.InteriorBorder_All,
              dune.common.CommunicationDirection.Forward)`
-   Some other auxiliary methods have been added, e.g.,
    -   `entity.domain` and `geometry.domain` to recover the reference element
    -   `grid.vtkWriter` and `grid.subSamplingVTKWriter` to write vtk files
    -   `grid.plot()` and `grid.plot(gridFunction)` for `matplotlib` figures
-   `dune-python` provides some grid function support using the
    `localFunction` concept with `bind` and `unbind`:

    ~~~
    lf = gf.localFunction()
    lf.bind(entity)
    y = lf(hatx)
    lf.unbind()
    ~~~

    The easiest way to construct grid functions is using the `gridFunction` decorator:

    ~~~
    @dune.grid.gridFunction
    def f(x): x[0]*x[1]
    ~~~

    The decorator adds the `localFunction` method to `f`. In addition it is still possible
    to write `f(x)` where `x` is a global coordinate but also `f(entity,hatx)` for evaluation
    in local coordinates. All `__call__` methods are vectorized, i.e., can be used efficiently
    with a `numpy` coordinate vector. The decorator can also be used to decorate functions with a
    `entity,hatx` signature (in this case no global evaluation is possible). Class having either
    a method `__call__(self,x)` or `__call__(self,entity,x)` can be decorated using 
    `@dune.grid.GridFunction` which adds the `localFunction` method.
